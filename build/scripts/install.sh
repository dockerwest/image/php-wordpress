#!/bin/sh

set -e

. /etc/os-release

# install packages
apt-get update

apt-get install -y default-mysql-client

extensions -i gd intl imagick mbstring mysql redis soap xml

apt-get clean -y

# install wpcli
curl -sS -o /usr/local/bin/wp \
    https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
chmod +x /usr/local/bin/wp

# update permissions to allow rootless operation
/usr/local/bin/permissions
